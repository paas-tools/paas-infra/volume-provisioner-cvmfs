/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"regexp"

	volumeprovisioner "gitlab.cern.ch/paas-tools/volume-provisioner-package"
	"k8s.io/api/core/v1"
	storage_v1 "k8s.io/api/storage/v1"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// CVMFSVolumeProvisioner represents a VolumeProvisioner for CVMFS volumes.
// This type takes a point to a v1.PersistentVolumeClaim and a CVMFS repositoryTag
// in the format of "<DNS subdomain>[@tag] or <DNS subdomain>[#hash]".
// The format of repositoryTag is validated before provisioning and the operation
// aborted if malformed
type CVMFSVolumeProvisioner struct{}

func NewCVMFSVolumeProvisioner() *CVMFSVolumeProvisioner {
	return &CVMFSVolumeProvisioner{}
}

// provisionVolume dynamically creates the volume share for the PVC. For CVMFS requests
// there is nothing to provisioning so the method just returns `nil`
func (vp *CVMFSVolumeProvisioner) provisionVolume() error {
	return nil
}

// CreatePV takes care of creating a CVMFS v1.PersistentVolume using a hostPath to `/cvmfs/<repositoryTag>`.
// If repositoryTag has an invalid format or the provisioning fails, raise an error
func (vp *CVMFSVolumeProvisioner) CreatePV(pvc *v1.PersistentVolumeClaim, storageClass *storage_v1.StorageClass) (*v1.PersistentVolume, error) {

	cvmfsPath, err := vp.parseRepositoryTag(pvc, storageClass.Parameters["repository"])
	if err != nil {
		return nil, err
	}

	// Provision the volume according to the requirements of the v1.PersistentVolumeClaim.
	// For CVMFS there is nothing to do however, as volumes are already provisioned and
	// mounted in the OpenShift servers
	if err := vp.provisionVolume(); err != nil {
		return nil, err
	}
	// Create CVMFS PV and bind it to existing PVC
	pv := &v1.PersistentVolume{
		ObjectMeta: meta_v1.ObjectMeta{
			GenerateName: fmt.Sprintf("pv-%s", *volumeprovisioner.GetStorageClassName(pvc)),
		},
		Spec: v1.PersistentVolumeSpec{
			Capacity: v1.ResourceList{
				v1.ResourceName(v1.ResourceStorage): pvc.Spec.Resources.Requests[v1.ResourceName(v1.ResourceStorage)],
			},
			// CVMFS needs to be read only
			AccessModes: []v1.PersistentVolumeAccessMode{v1.ReadOnlyMany},
			PersistentVolumeSource: v1.PersistentVolumeSource{
				HostPath: &v1.HostPathVolumeSource{
					Path: fmt.Sprintf("/cvmfs/%s", *cvmfsPath),
				},
			},
			ClaimRef: &v1.ObjectReference{
				Kind:      "PersistentVolumeClaim",
				Name:      pvc.GetName(),
				Namespace: pvc.GetNamespace(),
			},
		},
	}
	// Return the volume. Please note it has not been saved yet so at this point it
	// still does not exist
	return pv, nil
}

// UnprovisionPV removes the physical volume associated with the PV. For CVMFS volumes
// there is nothing to do so the funcion just returns nil
func (vp *CVMFSVolumeProvisioner) UnprovisionPV(pv *v1.PersistentVolume) error {
	return nil
}

// GetProvisionerName obtain name of this provisioner. This VolumeProvisioner will only act
// on PVCs whose StorageClass has this provisioner parameter
func (vp *CVMFSVolumeProvisioner) GetProvisionerName() string {
	return "cern.ch/volume-provisioner-cvmfs"
}

// parseRepositoryTag check if the repositoryTag from the volume provisioner follow
// format compatible with cloud team's docker-volume-cvmfs
// (https://gitlab.cern.ch/paas-tools/storage/docker-volume-cvmfs).
// A valid repository must consist of a DNS subdomain (http://tools.ietf.org/html/rfc3986#section-3.2.2)
// with at least 2 components,  optionally followed by @tag or #hash (if not specified,
// "trunk" tag is assumed). If valid, forms the cvmfs path for the specified repositoryTag.
// Returns an error if not.
func (vp *CVMFSVolumeProvisioner) parseRepositoryTag(pvc *v1.PersistentVolumeClaim, repositoryTag string) (*string, error) {

	subdomainRegexp := "[A-Za-z0-9](?:[A-Za-z0-9-]{0,61}[A-Za-z0-9])?"
	//<subdomain_re>(.<subdomain_re>)*([@#]<subdomain_re>)?
	// Use intermediate variable`subdomainRegexp` to make regexp easier to red
	fullRegexp := fmt.Sprintf("^(?P<repository>%[1]s(?:\\.%[1]s)*)(?:[@#]*(?P<subPath>%[1]s))?$",
		subdomainRegexp)
	re, err := regexp.Compile(fullRegexp)
	if err != nil {
		return nil, err
	}
	// Verify the repositoryTag is valid by using the regexp
	match := re.FindStringSubmatch(repositoryTag)
	if match == nil {
		return nil, fmt.Errorf("The repository tag is not valid. Please verify the annotation of PVC %s/%s",
			pvc.GetNamespace(), pvc.GetName())
	}

	// Extract named groups from repositoryTag  in the format of map[group] -> value
	// The values are:
	// - repository (e.g. something.cern.ch)
	// - subPath (e.g. tag or id to use)
	result := make(map[string]string)
	for i, name := range re.SubexpNames() {
		if i != 0 {
			result[name] = match[i]
		}
	}
	// Assume 'trunk' if subPath is empty
	if result["subPath"] == "" {
		result["subPath"] = "trunk"
	}
	cvmfsPath := fmt.Sprintf("%s/%s/", result["repository"], result["subPath"])
	return &cvmfsPath, nil
}
