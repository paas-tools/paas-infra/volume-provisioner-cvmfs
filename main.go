/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	goflag "flag"

	"github.com/golang/glog"
	flag "github.com/spf13/pflag"
	"gitlab.cern.ch/paas-tools/volume-provisioner-package"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Dry run mode
var dryRun bool

func main() {
	var namespace string

	flag.StringVar(&namespace, "namespace", "", "Namespace to watch. Do not set to watch all namespaces")
	flag.BoolVar(&dryRun, "dry-run", false, "Set flag to only print the operations instead of actually doing them")
	flag.Parse()

	// Convinces goflags that we have called Parse() to avoid noisy logs.
	// See https://github.com/kubernetes/kubernetes/issues/17162
	goflag.Set("logtostderr", "true")
	goflag.CommandLine.Parse([]string{})

	// creates the connection
	config, err := rest.InClusterConfig()
	if err != nil {
		glog.Fatal(err)
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Fatal(err)
	}

	cvmfsVolumeProvisioner := NewCVMFSVolumeProvisioner()
	stop := volumeprovisioner.StartVolumeProvisoner(cvmfsVolumeProvisioner, namespace, clientset, dryRun)
	defer close(stop)

	// Wait forever
	select {}
}
